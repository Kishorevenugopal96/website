<!DOCTYPE html>
<html>

<head>
    <title>CapFront Technologies-About Us</title>

    <!-- META TAGS STARTS -->
    <?php include_once('./includes/meta-tags.php');?>
    <!-- META TAGS ENDS -->
    <!-- CSS,JS FILES STARTS -->
    <?php include_once('./includes/head.php');?>
    <!-- CSS,JS FILES ENDS -->

    <link rel="stylesheet" href="aboutmobile.css">
    <link rel="stylesheet" href="exp3.css">
    <style>
        #active_about{
        text-decoration: none;
        font-weight:bold;
        color: #15a2f3 !important;
      }
      .active_service{
          color:white;
      }
  
    </style>
</head>

<body>

    <header id="headers" class="">
       <!-- MOBILE NAVIGATION STARTS -->
       <?php include_once("./includes/mobile-navigation-bar.php") ; ?>
      <!-- MOBILE NAVIGATION ENDS -->
        <div class="about-title">
            <p>About Us</p>
        </div>
    </header><!-- End Header -->
   <!-- DESKTOP NAVIGATION STARTS -->
   <?php include_once("./includes/desk-top-navigation-bar.php");?>
   <!-- DESKTOP NAVIHATION ENDS -->
    <br><br><br><br>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-6 p-0">
                    <img src="./img/Mask Group 8@2x.png" class="aboutus-firstsection-image1">
                    <img src="./img/Mask Group 9@2x.png" class="aboutus-firstsection-image2">
                    <div class="aboutus-120-div">
                        <div class="aboutus-120-div1"></div>
                        <div class="aboutus-120-div2">
                            <div class="aboutus-120-div2-text1">120</div>
                            <div class="aboutus-120-div2-text2" style="margin-left: 6px;">
                                <p class="para-aboutus">We have more than Employees</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <h2 class="aboutus-section1-right-heading">About Company</h2>
                    <p class="aboutus-section1-right-text">
                        Established in 2018, <b>CapFront Technologies</b> is a privately held information technology startup
                        based out of Bengaluru, India. The company offers product development and information technology
                        services to financial institutes.
                    </p>
                </div>
            </div>
        </div>
    </section>

    <br><br><br><br>
    <section>
        <div class="main-partss">
            <div class="main-div">
                <div class="container">
                    <div class="row main-row">
                        <div class="col-md-6 first-colmd-6">
                            <h1 class="image1-div-head">Why people choose us</h1>
                            <p class="image1-div-text" style="margin-left: 23px;">CapFront is a preferred go-to partner
                                for our clients as we believe we can
                                solve any complex problems through our hold on technology, and data analytics.</p>
                            <div class="image1-div">
                                <div class="image1-subdiv">
                                    <img src="./img/Group 200@2x.png" alt="" class="image1"
                                        style="margin-left: 90px; text-align: center;">
                                </div>
                                <div class="img2-subdiv">
                                    <img src="./img/Group 202.svg" alt="" class="image2"
                                        style="margin-left: 70px; text-align: center;">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 second-colmdd">
                            <div class="row sub-row">
                                <div class="col-6 sub-first-colmdd-6" style="margin-bottom: 79px;">
                                    <div class="sub-first-colmdd-6-subdiv">
                                        <div class="path299-image-div-2">

                                        </div>
                                        <p class="aboutus-card-head">Guarantee</p>
                                        <p class="aboutus-card-text">We guarantee efficient solutions that can change
                                            the way the
                                            FinTech market is evolving currently.</p>
                                    </div>
                                </div>
                                <div class="col-6 sub-second-colmdd-6" style="margin-bottom: 79px;">
                                    <div class="sub-second-colmdd-6-subdiv">
                                        <div class="path299-image-div-1">

                                        </div>
                                        <p class="aboutus-card-head">Speed</p>
                                        <p class="aboutus-card-text">We focus on solving complex problems quickly and
                                            efficiently
                                            to allow a seamless consumer experience.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row sub-row second-card-row">
                                <div class="col-6 sub-first-colmdd-6 about-imgs">
                                    <div class="sub-first-colmdd-6-subdiv">
                                        <div class="path299-image-div-3">

                                        </div>
                                        <p class="aboutus-card-head">reliability</p>
                                        <p class="aboutus-card-text">We can be relied upon on meeting our deadlines and
                                            commitments to any project we undertake.</p>
                                    </div>
                                </div>
                                <div class="col-6 sub-second-colmdd-6 about-imgs">
                                    <div class="sub-second-colmdd-6-subdiv">
                                        <div class="path299-image-div-4">

                                        </div>
                                        <p class="aboutus-card-head">Exprience</p>
                                        <p class="aboutus-card-text">We have experienced and skilled professionals who
                                            would
                                            go beyond their call of duty to achieve the larger enterprise goal.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <br><br>

    <!-- <section>
        <div class="container">
            <div class="col-md-12 aboutus-details">
                <h2 class="aboutus-h2">We Craft Elegent Solutions with</h2>
                <h2 style="color: #0078BC; margin-bottom:40px;" class="powerful-about">Powerful Technology</h2>
                <p class="aboutus-para-1" style="color:#707675;">
                    At CapFront, we leverage modern technology and machine learning techniques to assess risk through
                    various
                    alternate data sources and provide high quality technology services in the unsecured <b>short-term
                        and
                        micro-lending business</b> segments. Data analytics forms an important part of our strategy in
                    delivering
                    high-quality solutions. Our data analytics relies on the simultaneous application of statistics,
                    computer programming and operations research to quantify performance and communicate insight.
                    Our real time data analytics acts as a game changer for risk analytics and predictive analytics.
                </p>
            </div>
        </div>
    </section> -->
    
    <section>
        <div class="container">
            <div class="col-md-12">
                <h2>Core <b style="color: #0078BC;">Team</b></h2>
                <br>
                <p style="color: #425662;">
                    <!-- CapFront Technologies is focused on providing unparalleled analytics and digital marketing services,
                    product development and information technology services to financial institutes <b
                        style="color: #0078BC;">
                        (Banks and NBFCs….etc., )</b>
                    that supports application based lending. CapFront Technologies is a privately held information
                    technology
                    startup company based in Bengaluru, India. -->
                When your aim is to be better, faster and smarter, you need the best people driving your vision
                forward. CapFront is driven by a bunch of talented, and experienced professionals with diverse 
                backgrounds such as <b>IT, BFSI, Data Analytics, RISK, and Marketing,</b> to ensure efficient and 
                effective deliverables to our clients and consumers.
                </p>
                <br>
                <div class="row">
                    <div class="col-md-3" style="text-align: center;">
                        <img alt="<?php echo $team["CEO_PROFILE_IMAGE"]?> CEO of CapFront Technologies" src="<?php echo $team["CEO_PROFILE_IMAGE"]?>">
                        <h3 style="color: #0078BC;"><b><?php echo $team["CEO_NAME"]?></b></h2>
                            <h6>CEO</h6>
                    </div>
                    <div class="col-md-9 m-auto">
                        <p style="text-align: justify;color: #425662; line-height: 25px;">
                            Our Founder and CEO, Mr. <?php echo $team["CEO_NAME"]?>, is a seasoned professional with 19+ years of
                            experience in Software development, Product/portfolio management, Human resource and
                            Operations management in various domains including financial services.
                        </p>
                        <p style="text-align: justify;color: #425662; line-height: 25px;">
                            His personal skills and determination lead him through different roles over 19 years
                            of his career with various Technologyy based organizations. He progressed steadily
                            from Software Engineer, Senior Software Engineer, Technical Project Lead,
                            Senior Project Lead to other Senior management positions.
                        </p>
                        <p style="text-align: justify;color: #425662; line-height: 25px;">
                            Gowrinath’s vision is to use Technology to address gaps and improve efficiency in
                            financial services domain, while keeping the products easy to use and hassle-free for
                            the common Indian.In the month of November 2018, CapFront Technologies became a reality
                            and Launched its first product, a financial services platform "LoanFront - an easy to use,
                            quick & efficient financial services platform for Indians."
                        </p>
                    </div>
                </div>
                <br><br>
                <div class="row">
                    <div class="col-md-9 m-auto">
                        <p style="text-align: justify;color: #425662;line-height: 25px;">
                        <?php echo $team["SOFTWARE_ARCHITECT_NAME"]?> brings in an experience of more than 14 years working in the IT Industry.
                            He has played varied roles in his 12 years of rich experience with Huawei Technologies.
                            He has played various roles in his previous experience and has successfully completed
                            many mission-critical projects as a Leader as well as a member of various teams.
                        </p>
                        <p style="text-align: justify;color: #425662;line-height: 25px;">
                            He completed his Master of Engineering from Bangalore University (U.V.C.E) in Information
                            Technology.
                        </p>
                        <p style="text-align: justify;color: #425662;line-height: 25px;">
                            He brings in a unique blend of Business and Technology understanding. His area of expertise
                            is Data Analytics and Software Development.
                        </p>
                    </div>
                    <div class="col-md-3" style="text-align: center;">
                        <img alt="<?php echo $team["SOFTWARE_ARCHITECT_PROFILE_IMAGE"]?> Software Architect of CapFront Technology" src="<?php echo $team["SOFTWARE_ARCHITECT_PROFILE_IMAGE"]?>">
                        <h3 style="color: #0078BC;"><b> <?php echo $team["SOFTWARE_ARCHITECT_NAME"]?></b></h2>
                            <h6>Software Architect</h6>
                    </div>
                </div>
                <br><br>
                <div class="row">
                    <div class="col-md-3" style="text-align: center;">
                        <img alt="<?php echo $team["TECH_MANAGER_PROFILE_IMAGE"] ?> CTO of CapFront Technologies" src="<?php echo $team["TECH_MANAGER_PROFILE_IMAGE"] ?>">
                        <h3 style="color: #0078BC;"><b><?php echo $team["TECH_MANAGER_NAME"] ?></b></h2>
                            <h6>CTO</h6>
                    </div>
                    <div class="col-md-9 m-auto">
                        <p style="text-align: justify; color: #425662;line-height: 25px;">
                            <?php echo $team["TECH_MANAGER_NAME"] ?> is a young computer science master with a towering repertoire built over a span
                            of 13 years. He is adept at multiple technology and solution platforms and has an unswerving
                            passion to explore newer technologies while also encouraging startups to do the same. His
                            relentless pursuit in putting his inordinate amount of programming skills to design complex,
                            distributed systems while striving to keep them simple has catapulted Capfront to greater
                            heights. His ability to optimize source-code is impeccable and he tenaciously strives to
                            explore newer optimization methods thus saving the time and energy of his co-developers,
                            while also opening new doors for them. Besides being a master in technology, he has a
                            deep understanding of finance and business.
                        </p>
                        <p style="text-align: justify; color: #425662;line-height: 25px;">
                            He quit his promising career at TATA Elxsi and turned down offers from several other
                            multinational giants after having decided to use his broad corporate experience to aid
                            fledgling startups.
                        </p>
                    </div>
                </div>
                <br><br>
                <div class="row">
                    <div class="col-md-9 m-auto">
                        <p style="text-align: justify; color: #425662;line-height: 25px;">
                            <?php echo $team["COO_NAME"] ?> comes with 22 years of experience in IT services, product development,
                            maintenance & support
                            with specialization in Operations, IT Governance & Consulting, Program Management and
                            Quality Management.
                            He joined CapFront after having been associated with companies like Hewlett-Packard, Wirpro,
                            Cyient and
                            BirlaSoft. He served in various leadership roles in India and delivered overseas consulting
                            assignments.
                        </p>
                        <p style="text-align: justify; color:#425662;line-height: 25px;">
                            Satya has extensive experience and expertise in areas relating to establishing start-ups and
                            CoEs/ODCs,
                            P&L management, Budgeting and Controlling, Transition & Transformation, Supplier Evaluation
                            & Improvement,
                            Information security, policy and process development and establishing Management Systems in
                            multiple
                            sectors/industry segments including Financial services, eCommerce, digital transformation
                            services etc.
                        </p>
                        <p style="text-align: justify; color:#425662;line-height: 25px;">
                            Satya holds a post-graduate degree in Industrial Engineering & Management from JNTU,
                            Hyderabad and a post
                            diploma in Management with various professional certifications.
                        </p>
                    </div>
                    <div class="col-md-3" style="text-align: center;">
                        <img alt="<?php echo $team["COO_PROFILE_IMAGE"] ?> COO of CapFront Technology" src="<?php echo $team["COO_PROFILE_IMAGE"] ?>" class="satya-image">
                        <h3 style="color: #0078BC;"><b><?php echo $team["COO_NAME"] ?></b></h2>
                            <h6>COO</h6>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--ourteam container row start-->

    <!-- <div class="container-slider">
<div class="next">
<img src="./img/nextyy.png">
</div>
<div class="prev">
    <img src="./img/next.png">
</div>
<div class="autoplay">
<div class="slidder-box">
    <img src="./img/Component25.png" class="ourteam-image">
    <h3 class="ourteam-image-div-heading">IVR Gowrinath</h2>
        <h4 class="ourteam-image-div-h4">CEO</h4>
        <p class="ourteam-image-div-paragrph">Our Founder and CEO, Mr. Gowrinath Raghava, is a seasoned professional
            with 19+ years of experience in  Software development, Product/portfolio management, Human resource
                and Operations management in various domains including financial services.</p>

        <div class="ourteam-sm-icons-div">
            <div class="img-box-24">
            </div>                        
            <div class="img-box-25">
            </div>
            <div  class="img-box-26">
            </div>
        </div>
</div>
<div class="slidder-box">
    <img src="./img/Component26.png" class="ourteam-image">
    <h3 class="ourteam-image-div-heading">Salsan Jose</h3>
    <h4 class="ourteam-image-div-h4">Tech Manager</h4>
    <p class="ourteam-image-div-paragrph">Salsan Jose is a young computer science master with 
        a towering repertoire built over a span of 13 years. He is adept at multiple technology 
        and solution platforms and has an unswerving passion to explore newer technologies 
        and Operations management in various domains</p>
        <div class="ourteam-sm-icons-div">
            <div class="img-box-24">
            </div>                        
            <div class="img-box-25">
            </div>
            <div  class="img-box-26">
            </div>
        </div>
</div>
<div class="slidder-box">
    <img src="./img/component20.png" class="ourteam-image">
    <h3 class="ourteam-image-div-heading">Prudhivi Kodali</h3>
    <h4 class="ourteam-image-div-h4">Software Architect</h4>
    <p class="ourteam-image-div-paragrph"> Prudhvi Kodali brings in an experience of more than 14 years working in the IT Industry.
        He has played varied roles in his 12 years of rich experience with Huawei Technologies.
        He has played various roles in his previous experience and has successfully completed</p>
    <div class="ourteam-sm-icons-div">
        <div class="img-box-24">
        </div>                        
        <div class="img-box-25">
        </div>
        <div  class="img-box-26">
        </div>
    </div>
</div>
</div>
</div> -->

    <!--mapping image part end-->

    <?php include_once('./includes/footer.php') ; ?>

</body>

</html>