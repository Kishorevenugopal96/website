<!DOCTYPE html>
<html>

<head>
<title>CapFront Techonlogy-HomePage</title>
<script src="capfronthome.js"></script>
   
   <!-- META TAGS STARTS -->
    <?php  include_once("./includes/meta-tags.php"); ?>
    <!-- META TAGS ENDS -->

    <!-- JS,CSS LINKS STARTS -->
    <?php  include_once("./includes/home-page-head.php");?>
    <!-- JS,CSS LINKS ENDS -->
    <!-- GOOGLE RECAPTCHA STARTED -->
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
   <!-- GOOGLE RECAPTCHA ENDS -->
  <style>
      .warnMessage{
          color: red;
          font-size: 14px;
          padding-top: 10px;
          font-weight: bold;
      }
      .active_home{
        color: #15a2f3;
        font-weight: bold;
        margin: 0px 20px;
      }
      
  </style>
</head>
   
<body>
    
   <!-- <heading Part start > -->
    <header id="header" class=""> 
      <?php include_once("includes/home-page-navigation-bar.php"); ?>
    </header>
    <!-- End Part Header -->


     <!-- container-heading start -->
     <div class="header-div-1">
         <div class="header-parts">
             <br><br>
             <h3 class="need-parts">Why CapFront?</h3>
             <div class="work-process-pogressbar">
                <hr class="process-accessory">
            </div>
            <br>
            <p style="text-align: center; color: white; letter-spacing: 1px;" class="para-home">
            It is a one-stop solution for all FinTech needs catering to finanicail institutions including <b>Banks / NBFCs,</b>
            <br>and also running a digital platform to give out quick and easy loans.
            </p>
            <br>
            <!-- <div style="text-align: center;">
                <button type="button" class="button-header">Read More</button>
            </div> -->
            <br><br>
         </div>
     </div>
     <br><br>
      <!-- container-heading End -->
    <!--start of mobile image container-->
    <div class="container image-container-div mt-5 p-0">
        <div class="div1">
            <h1 class="w-100 colmd6content1h1">who we are and what we do ?</h1>
            <div class="progress colmdcontent6pogressbar">
                <div class="progress-bar bg-scondary" role="progressbar" style="width: 100%" aria-valuenow="25"
                    aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <p class="colmd6content1p"><strong class="footer-spann">
                CapFront Technologies</strong> is a privately held information technology start-up based in Bengaluru, India.At CapFront,
            </p>
        </div>
        <div class="div2">
            <div class="colmd6content1ul">
                <p class="colmd6content1li"><img src="./img/Group 2.svg" style="margin: 5px;">We leverage modern technology for providing unparalleled <strong>Data Analytics.</strong></p>
                <p class="colmd6content1li"><img src="./img/Group 2.svg" style="margin: 5px;">Automated platform services,</p>
                <p class="colmd6content1li"><img src="./img/Group 2.svg" style="margin: 5px;">Product development and Machine learning techniques.</p>
                <p class="colmd6content1p">
                to assess risk through various alternate data sources, to provide high quality technology services 
                in the unsecured short-term and <b>micro-lending business</b> segments. 
                </p>
            </div>
            <a href="service.php"><button class="colmd6content1button">Read More</button></a>
        </div>
        <div class="div3">
            <img alt="Capfront Technologies Will provided the Business Services." src="./img/mobile 4.png" class="imagegrid img-fluid">
        </div>
    </div>
    <!--End mobile image container-->
    <!-- Start service container start-->
    <div class="servicecontner">
        <h1>Our Services</h1>
        <div class="servicecontnerpogressbar">
            <hr class="service-accessory">
        </div>
        <p class="servicecontner-paragrph">From concept to actual delivery to periodic maintenance, we strive to
         provide a smooth and bumpy-free ride to our consumers through our various features.</p>
    </div>
    <!-- End service container start-->
      <!--service container start-->
    <div class="container mt-5">
        <div class="row">
            <div class="col-md-3 text-center service-images m-auto">
                <div class="serviceimge-div">
                    <img alt="UI/UX to design the Template as Requirement" src="./img/supply.svg" class="serviceimages">
                    <img alt="UI/UX to design the Template as Requirement"  src="./img/Group 45.svg" class="serviceimageshover">
                    <!-- <img class="mr-3 feature-images" src="./img/supply.svg" alt="Generic placeholder image"> -->
                </div>
                <h1 class="service-images-sub-heading ">UI/UX</h1>
                <div class="progress service-pogressbar">
                    <div class="progress-bar bg-white" role="progressbar" style="width: 100%" aria-valuenow="25"
                        aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <p class="service-images-sub-paragrph" style="padding-bottom: 0px;">
                <!-- Finding the consumer’s requirements, analysing user’s data and research, and creating a prototype to 
                deliver a highly effective user experience product. -->
                Pioneer in aesthetically designed user interfaces and designing more meaningful and intuitive customer 
                experience on various digital platforms by adopting user-cantered design (UCD) concepts.
                </p>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-3 text-center service-images m-auto">
                <div class="serviceimge-div">
                    <img alt="Product Engineering for new Devolopment" src="./img/computer.svg" class="serviceimages">
                    <img src="./img/computers.svg" class="serviceimageshover">
                </div>
                <h1 class="service-images-sub-heading ">Product Engineering</h1>
                <div class="progress service-pogressbar">
                    <div class="progress-bar bg-white" role="progressbar" style="width: 100%" aria-valuenow="25"
                        aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <p class="service-images-sub-paragrph" style="padding-bottom: 20px;">Designing the prototype with visuals, colours, information architecture, 
                animation, branding and graphic to make it user-friendly that appeals to consumers aesthetically.</p>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-3 text-center service-images m-auto">
                <div class="serviceimge-div">
                    <img src="./img/safe-1.svg" class="serviceimages">
                    <img src="./img/safe.svg" class="serviceimageshover">
                </div>
                <h1 class="service-images-sub-heading ">FrontEnd / Backend Development</h1>
                <div class="progress service-pogressbar">
                    <div class="progress-bar bg-white" role="progressbar" style="width: 100%" aria-valuenow="25"
                        aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <p class="service-images-sub-paragrph">
                Development of rich frontend & strong backend services, with awe view of the application and a highly 
                secured microservice based distributed arctitecture. 
                </p>
            </div>
        </div>
        <br><br>
        <div class="row">
            <div class="col-md-3 text-center service-images" style="margin-left: 25px;">
                <div class="serviceimge-div">
                    <img src="./img/desktop-2.svg" class="serviceimages">
                    <img src="./img/desktop.svg" class="serviceimageshover">
                </div>
                <h1 class="service-images-sub-heading ">Risk Analytics</h1>
                <div class="progress service-pogressbar">
                    <div class="progress-bar bg-white" role="progressbar" style="width: 100%" aria-valuenow="50"
                        aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <p class="service-images-sub-paragrph" style="padding-bottom: 40px;">
                Risk analytics is a core component of CapFront Technologies which provides business 
                intelligence in a risk management environment.
               </p>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-3 text-center service-images" style="margin-left: 25px;">
                <div class="serviceimge-div">
                    <img src="./img/paper-2.svg" class="serviceimages">
                    <img src="./img/paper.svg" class="serviceimageshover">
                </div>
                <h1 class="service-images-sub-heading ">Big Data Analytics</h1>
                <div class="progress service-pogressbar">
                    <div class="progress-bar bg-white" role="progressbar" style="width: 100%" aria-valuenow="25"
                        aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <p class="service-images-sub-paragrph" style="padding-bottom: 40px;">
                Complex process of examining big data to uncover hidden patterns, co-relations, 
                and market trends is simplified through inhouse Big data analytics solution. 
               </p>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-3 text-center service-images" style="margin-left: 25px;">
                <div class="serviceimge-div">
                    <img src="./img/computer-5.svg" class="serviceimages">
                    <img src="./img/computer 1.svg" class="serviceimageshover">
                </div>
                <h1 class="service-images-sub-heading ">Quality Control</h1>
                <div class="progress service-pogressbar">
                    <div class="progress-bar bg-white" role="progressbar" style="width: 100%" aria-valuenow="25"
                        aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <p class="service-images-sub-paragrph" style="padding-bottom: 40px;">
                Quality is of highest priority at CapFront. The Quality starts at design and ends at delivery. 
                </p>
            </div>
            <div class="col-md-1"></div>
        </div>
    </div>
    <!--service container end-->
    <!--our team container start-->
    <!--our team container start-->
    <div class="ourteam-base-div  p-0 margin-top">
        <h1 class="our-team-container-heading pt-5">Meet Our Team</h1>
        <div class=" our-team-pogressbar">
            <hr class="our-team-accessory">
        </div>
        <p class="our-team-container-paragrph">We are a bunch of talented, and experienced professionals from diverse fields such as data analytics, IT and marketing to ensure efficient and effective deliverables to our clients and consumers.
        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
    </div>

    <!--ourteam container row start-->

    <div class="container-slider">
        <div class="next">
            <img src="./img/nextyy.png">
        </div>
        <div class="prev">
                <img src="./img/next.png">
        </div>

        <div class="autoplay">
            <div class="slidder-box">
                <img src="<?php echo $team["CEO_PROFILE_IMAGE"]?>" class="ourteam-image">
                <div>
                <a href="<?php echo $team["CEO_SOCIAL_LINK"]?>" target="_blank"><img src="./img/linkedin.svg" class="ourteam-linkindin"></a>
                </div>
                <h4 class="ourteam-image-div-heading"><?php echo $team["CEO_NAME"]?></h4>
                    <h4 class="ourteam-image-div-h4">CEO</h4>
                    <p class="ourteam-image-div-paragrph">Our Founder and CEO, Mr. Gowrinath Raghava, is a seasoned professional
                        with 19+ years of experience in  Software development, Product/portfolio management, Human resource
                         and Operations management in various domains including financial services.
                    </p>
                         <div class="button">
                         <a href="<?php echo ABOUT_US_PAGE_LINK ?>"><button class="btn know-more btn-primary">Know More</button></a>
                        </div>
            </div>
            <div class="slidder-box">
                <img src="<?php echo $team["TECH_MANAGER_PROFILE_IMAGE"] ?>" class="ourteam-image salson-part">
                <div>
                <a href="<?php echo $team["TECH_MANAGER_SOCIAL_LINK"] ?>" target="_blank"><img src="./img/linkedin.svg" class="ourteam-linkindin"></a>
                </div>
                <h4 class="ourteam-image-div-heading"><?php echo $team["TECH_MANAGER_NAME"] ?></h4>
                <h4 class="ourteam-image-div-h4">Tech Manager</h4>
                <p class="ourteam-image-div-paragrph">Salsan Jose is a young computer science master with 
                    a towering repertoire built over a span of 13 years. He is adept at multiple technology 
                    and solution platforms and has an unswerving passion to explore newer technologies 
                    and Operations management.
                 </p>
                  <div class="button">
                  <a href="<?php echo ABOUT_US_PAGE_LINK ?>"><button class="btn know-more btn-primary">Know More</button></a>
                </div>
            </div>
            <div class="slidder-box">
                <img src="<?php echo $team["SOFTWARE_ARCHITECT_PROFILE_IMAGE"]?>" class="ourteam-image">
                <div>
                <a href="<?php echo $team["SOFTWARE_ARCHITECT_SOCIAL_LINK"]?>" target="_blank"><img src="./img/linkedin.svg" class="ourteam-linkindin"></a>
                </div>
                <h4 class="ourteam-image-div-heading"><?php echo $team["SOFTWARE_ARCHITECT_NAME"]?></h4>
                <h4 class="ourteam-image-div-h4">Software Architect</h4>
                <p class="ourteam-image-div-paragrph"> Prudhvi Kodali brings in an experience of more than 14 years working in the IT Industry.
                    He has played varied roles in his 12 years of rich experience with Huawei Technologies.
                    He has played various roles in his previous experience and has successfully completed.
                 </p>
                    <div class="button">
                    <a href="<?php echo ABOUT_US_PAGE_LINK ?>"><button class="btn know-more btn-primary">Know More</button></a>
                    </div>
            </div>
    
            <div class="slidder-box">
                <img src="<?php echo $team["COO_PROFILE_IMAGE"]?>" class="ourteam-image">
                <div>
                <a href="<?php echo $team["COO_SOCIAL_LINK"]?>" target="_blank"><img src="./img/linkedin.svg" class="ourteam-linkindin"></a>
                </div>
                <h4 class="ourteam-image-div-heading"><?php echo $team["COO_NAME"]?></h4>
                    <h4 class="ourteam-image-div-h4">COO</h4>
                   <p class="ourteam-image-div-paragrph">Satya M Reddy comes with 22 years of experience in IT services, product development, 
                    maintenance & support with specialization in Operations, IT Governance & Consulting, Program 
                    Management and Quality Management. He joined CapFront after having been associated.
                   </p>
                    <div class="button">
                        <a href="<?php echo ABOUT_US_PAGE_LINK ?>"><button class="btn know-more btn-primary">Know More</button></a>
                    </div>
            </div>

        </div>
        <!--ourteam container row end-->
    </div>



    <!--ourteam container row end-->

    <!--our team container end-->

    <!--contact us container start-->

    <div class="container p-5 contactcontainer">
        <h2 class="contactush5">Contact Us</h2>
        <div class="contact-pogressbar">
            <hr class="contact-accessory">
        </div>
    </div>

     <!--contact us container End-->
    
    <div class="container card-container">
        <div class="card card-div">
            <div class="card-body">
                <div class="media">
                    <div class="align-self-center mr-3">
                        <div class="imgBox">
                        </div>
                    </div>
                    <div class="media-body contactus-subdiv">
                        <h1 class="mt-0 contactus-subdiv-heading">Address</h1>
                        <p class="contactus-subdiv-paragrph">
                           <?php echo ADDRESS_LINE1 ?> ,<br>
                            <?php echo ADDRESS_LINE2 ?> ,<br>
                            <?php echo ADDRESS_LINE3 ?> ,<br>
                            <?php echo CITY  ?> -  <?php echo STATE ; ?> <?php echo PINCODE ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="card card-div">
            <div class="card-body mt-3">
                <div class="media">
                    <div class="align-self-center mr-3">
                        <div class="imgBox-1">
                        </div>
                    </div>
                    <div class="media-body contactus-subdiv">
                        <h4 class="mt-0 contactus-subdiv-heading">Email</h4>
                    <p>
                        <a  class="contactus-subdiv-paragrph" href="mailto:<?php echo  CAPFRONT_OFFICIAL_EMAIL?>;" style="text-decoration:none;"><?php echo  CAPFRONT_OFFICIAL_EMAIL ;?></a> <br>
                        <a class="contactus-subdiv-paragrph" href="mailto:<?php echo CAPFRONT_GMAIL_EMAIL ?>" style="text-decoration:none;"><?php echo CAPFRONT_GMAIL_EMAIL ?></a>
                    </p>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="card card-div">
            <div class="card-body mt-3">
                <div class="media">
                    <div class="align-self-center mr-3">
                        <div class="imgBox-2">
                        </div>
                    </div>
                    <div class="media-body contactus-subdiv">
                        <h4 class="mt-0 contactus-subdiv-heading">Phone</h4>
                        <a href="tel: <?php echo CAPFRONT_LANDLINE_NUMBER;?>;" style="text-decoration:none;"><p class=" contactus-subdiv-paragrph">
                            <?php echo CAPFRONT_LANDLINE_NUMBER;?>
                        <br></p></a>
    
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- CONTACT FORM STARTS -->
    <?php include_once("home_page/contact_form.php"); ?>
    <!-- CONTACT FORM ENDS -->
    
    <!--contactus form-field end-->

    <!--mapping image part start-->

   
    <!-- FOOTER STARTS -->
  <?php include_once("includes/footer.php"); ?>
    <!-- FOOTER ENDS -->
    
    <script>
        function showErrorMessage(id,message,paraId){
            $(id).css('border', '1px solid red');
            $(paraId).html(message);
        }
    </script>    
   <script>
       $(document).ready(function(){
           /* 
            Note : All error messages are defined in config.php file
           */

           // Slick code starts

          $('.autoplay').slick({
     slidesToShow: 1,
     slidesToScroll: 1,
     mobileFirst: true,
     autoplay: false,
     autoplaySpeed: 2000,
       nextArrow: $('.next'),
       prevArrow: $('.prev'),
     responsive: [
           {
                   breakpoint: 768,
                   settings: 
                   {
                    slidesToShow: 4,
                    slidesToScroll: 4
                   }
           },
           {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
     ]
   });
            // Slick ends


          $("#sendBtn").click(function(e){

            var isFormValid = false;

            var nameRegex = '^[A-Za-z ]{3,100}$';
            var emailReg = '^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$';

            var name = $("#name").val();
            var email = $("#email").val();
            var message = $("#message").val();

            $(".warnMessage").each(function(){
                $(this).html("");
            });
           
            if (name.match(nameRegex)) {
                isFormValid = true;
                $("#nameWarning").html("");
                $("#name").css('border', '1px solid #0078BC');
            }

            if (email.match(emailReg)) {
                isFormValid = true;
                $("#nameWarning").html("");
                $("#email").css('border', '1px solid #0078BC');
            }

            if(message != ""){
                isFormValid = true;
                $("#nameWarning").html("");
                $("#message").css('border', '1px solid #0078BC');
            }

            if (!name.match(nameRegex)) {
                isFormValid = false;
                var id = "#name";
                var paraId = "#nameWarning";
                var message = "<sup>*</sup> <?php echo NAME_ERROR_MSG; ?>";
                showErrorMessage(id,message,paraId);
                e.preventDefault();
            } 

            if (!email.match(emailReg)) {
                isFormValid = false;
                var id = "#email";
                var paraId = "#emailWarning";
                var message = "<sup>*</sup> <?php echo EMAIL_ERROR_MSG; ?>";
                showErrorMessage(id,message,paraId);
                e.preventDefault();

               
            } 

            if(message == ""){
                isFormValid = false;
                var id = "#message";
                var paraId = "#messageWarning";
                var message = "<sup>*</sup> <?php echo MESSAGE_ERROR_MSG; ?>";
                showErrorMessage(id,message,paraId);
                e.preventDefault();  

            } else if(message.length < 15){
                isFormValid = false;
                var id = "#message";
                var paraId = "#messageWarning";
                var message = "<sup>*</sup> <?php echo MESSAGE_LENGTH_ERROR_MSG; ?>";
                showErrorMessage(id,message,paraId);
                e.preventDefault(); 
                
            } 

            if(isFormValid == true){
                $("#resultarea").html(`<span style="color:#0078BC">Please wait..</span>`);
                $.ajax({
                        type: "POST",
                        url: "process/homePageForm.php",
                        data: {name:name,email:email,message:message},
                        cache: false,
                        success: function(data){
                            var response = JSON.parse(data);
                            if(response.code != 200){
                                $("#resultarea").html(`<span style="color:red">${response.message}</span>`);
                            } else{
                                $("#resultarea").html(`<span style="color:green">${response.message}</span>`);
                            }
                }
});
            }

          });
       });
   </script>

   
</body>

</html>