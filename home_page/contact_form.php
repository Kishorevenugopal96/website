<div class="form-div">
        <div class="row m-1 ">
            <div class="col-md-6">
                <form class="form-group" id="form">
                    <input type="text" placeholder="Name" class="form-control form-control-lg" id="name">
                    <p class="warnMessage" id="nameWarning"></p>
                </form>
            </div>
            <div class="col-md-6">
                <form class="form-group">
                    <input type="email" placeholder="Email" class="form-control form-control-lg" id="email">
                    <p class="warnMessage" id="emailWarning"></p>
                </form>
            </div>
        </div>
        <div class="row m-1">
            <div class="col-md-12">
                <div class="form-group">
                    <textarea rows="6" class="form-control form-control-lg" placeholder="Message" id="message"></textarea>
                    <p  class="warnMessage" id="messageWarning"></p>
                </div>

            </div>
        </div>
        <div class="text-center">
            <button class="form-btn" id="sendBtn">SEND MESSAGE</button> <br> <br>
            <p id="resultarea"></p>
        </div>
    </div>
    