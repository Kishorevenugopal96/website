<?php
// GOOGLE RECAPTCHA  STARTS
//https://jalfuatapi.loanfront.in/lf_service/data
define("GOOGLE_RECAPTCHA_SECRETE_KEY","6LfggCYcAAAAAPefzHd1WhmF4J8UzIe6TKvJ4mYV");
define("URL","https://samruddhi-lucky-draw.herokuapp.com/lf_service/data");
// GOOGLE RECAPTCHA  ENDS


define("HOME_PAGE_LINK", "index.php");
define("ABOUT_US_PAGE_LINK", "about.php");
define("OUR_SERVICE_PAGE_LINK", "service.php");
define("OUR_PRODUCTS_PAGE_LINK", "product.php");
define("CONTACT_US_PAGE_LINK", "contactus.php");

// ERROR MESSAGES STARTS

define("NAME_ERROR_MSG","Please enter your name");
define("EMAIL_ERROR_MSG","Please enter your email id");
define("MOBILE_ERROR_MSG","Please enter your valid 10 digit mobile number");
define("SUBJECT_ERROR_MSG","Please enter your subject");
define("MESSAGE_ERROR_MSG","Please enter your message ...");
define("VALID_MESSAGE_ERROR_MSG","Please enter valid message ...");
define("MESSAGE_LENGTH_ERROR_MSG","Message should be more that 15 characters");
define("SUBMIT_CAPTCHA","Please submit google captch");
define("CAPTCHA_VERIFICATION_FAILED","Captcha varification failed.");
// ERROR MESSAGES ENDS



define("COMPANY_LOGO","./img/logo.png");
define("COMPANY_FOOTER_LOGO","img/capfrontlogo.jpg");

// CONTACT INFO STARTS  

define("CAPFRONT_OFFICIAL_EMAIL" ,"info@capfront.in");
define("CAPFRONT_GMAIL_EMAIL", "capfront@gmail.in");
define("CAPFRONT_LANDLINE_NUMBER" , "080 4812 6351");

// CONTACT INFO ENDS

// COMPANY ADDRESS STARTS

define("ADDRESS_LINE1","No. 1, Second Floor");
define("ADDRESS_LINE2","Old Airport Road, Domlur Layout,");
define("ADDRESS_LINE3","Near Domlur Post Office");
define("ADDRESS_LINE4","Near Domlur Post Office"); 
define("CITY","Bangalore");
define("PINCODE","560071");
define("STATE","Karanataka");

// COMPANY ADDRESS ENDS 

//SOCIAL LINKS STARTS linkedin
define("CAPFRONT_OFFICIAL_LINKEDIN" ,"https://in.linkedin.com/company/capfront-india");
define("CAPFRONT_OFFICIAL_TWITTER" ,"https://twitter.com/GoLoanFront");
define("CAPFRONT_OFFICIAL_FACEBOOK" ,"https://www.facebook.com/capfronttech/");
// SOCIAL LINKS ENDS


// TEAM MEMBERS STARTS
$team = array( 
    
            "CEO_NAME" =>"IVR Gowrinath",
            "CEO_PROFILE_IMAGE" =>"./img/Component25.png",
            "CEO_SOCIAL_LINK" => "https://in.linkedin.com/in/capfront-ceo",

            "TECH_MANAGER_NAME" => "Salsan Jose",
            "TECH_MANAGER_PROFILE_IMAGE" => "./img/Component26.png",
            "TECH_MANAGER_SOCIAL_LINK" => "https://in.linkedin.com/in/salsanjose",

            "SOFTWARE_ARCHITECT_NAME" => "Prudhivi Kodali",
            "SOFTWARE_ARCHITECT_PROFILE_IMAGE" => "./img/component20.png",
            "SOFTWARE_ARCHITECT_SOCIAL_LINK" => "https://in.linkedin.com/in/prudhvikodali",

            "COO_NAME" => "Satya Malliya",
            "COO_PROFILE_IMAGE" => "./img/satya.jpg",
            "COO_SOCIAL_LINK" => "https://in.linkedin.com/in/satyamvreddy",


)
// TEAM MEMBERS ENDS




?>