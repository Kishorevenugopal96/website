<!DOCTYPE html>
<html>
   
<head>

    <title>CapFront Technologies-Our Product</title>
 <!-- META TAGS STARTS -->
 <?php include_once('./includes/meta-tags.php');?>
    <!-- META TAGS ENDS -->
     <!-- CSS,JS FILES STARTS -->
     <?php include_once('./includes/head.php');?>
    <!-- CSS,JS FILES ENDS -->
    <link href="img/capfrontlogo0-modified.png">    
    <link rel="stylesheet" href="product.css">
    <link rel="stylesheet" href="productmobile.css">
    <link rel="stylesheet" href="service.css">
    <link rel="stylesheet" href="servicemobile.css">
  
    <style>
        #active_product
        {
            font-weight:bold;
            color: #15a2f3 !important;
        }
        .active_about
        {
            
        }
    </style>
    
</head>

<body>
    

<header id="headers" class="">    
      <!-- MOBILE NAVIGATION STARTS -->
      <?php include_once("./includes/mobile-navigation-bar.php") ; ?>
      <!-- MOBILE NAVIGATION ENDS -->
    <div class="about-titlet">
        <p>Our Products</p>
    </div>
</header><!-- End Header -->  

  <!-- DESKTOP NAVIGATION STARTS -->
  <?php include_once("./includes/desk-top-navigation-bar.php");?>
   <!-- DESKTOP NAVIHATION ENDS -->

    <!-- Our Product -->
    <!-- <div class="servicepage-product-container col-md-12">
        <h1 class="servicepage-product-heading">Our Products</h1>
        <div class="servicecontnerpogressbar">
            <hr class="service-accessory">
            <p>
                It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. 
                The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ‘Content here, 
                content here’, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as 
                their default model text, and a search for ‘lorem ipsum’ will uncover many web sites still in their infancy. Various versions have 
                evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
            </p>
        </div>
        
        <div class="row mx-0 margin-top">
            <div class="col-md-5 servicepage-product-img-cols">
                <img src="./img/Group 1314.svg" class="servicepage-product-img">
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-5 servicepage-product-img-cols">
                <img src="./img/Group 1715.svg" class="servicepage-product-img">
            </div>
        </div>
    </div> -->

<div class="service-page">
<h1 class="servicepage-product-heading">Our Products</h1>
<div class="servicecontnerpogressbar">
    <hr class="service-accessory">
</div>
<div class="container">
<p class="para-long">
    As a <b style="color: #0078BC;">FinTech start-up</b>, CapFront specialises in producing solutions that meets the financial needs 
    of borrowers, while helping lenders an easy platform to lend and conduct their business. CapFront 
    has always aimed to create, produce and maintain solutions that would bridge the gap between the 
    lender and the borrower, and thus bringing in the financial inclusiveness. With this goal in mind, 
    CapFront has built a couple of exciting solutions that would make getting financial help a walk in 
    the park, or just by a click, literally.
</p>
</div>
</div>

<!-- <div class="container">
    <div class="row mx-0 margin-top">
        <div class="col-md-2"></div>
            <div class="col-md-3 col-sm-3 col-lg-3 servicepage-product-img-cols">
                <a href="https://www.loanfront.in/" target="_blank"><img src="./img/Group 1314.svg" class="servicepage-product-img"></a>
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-3 col-sm-3 col-lg-3 servicepage-product-img-cols">
                <a href="https://www.peoplefront.in/"><img src="./img/Group 1715.svg" class="servicepage-product-img"></a>
          </div>
    </div>
</div> -->
<br><br><br><br><br><br>

<section>
<div class="container">
    <div class="row">
        <div class="col-md-6">
        <div class="card-img-bottom">
        </div>
        <div class="card-images">
        <img alt="Money Requirement" src="./img/rupeeicon.svg" alt="Rs.2000-20,000" style="color:#0078BC;margin: 9px;"><span class="card-img">₹2000 - ₹2Lakh</span>
        <img alt="Loanfront Download App" src="./img/mobile_icon.svg" alt="Quick Approvel From LoanFront" style="color:#0078BC;margin: 9px;"><span class="card-img">Quick Approval</span>
        <img alt="Loanfront Download App" src="./img/flash icon.svg" alt="Paperless Product" style="color:#0078BC;margin: 9px;"><span class="card-img">Paperless Processing</span>
        </div>
        </div>
        <div class="col-md-6">
        <div class="card-block">
        <h2 class="card-title">About <b style="color: #0078BC;">LoanFront App</b></h2>
        <p class="card-text" style="color:#425662;line-height: 25px;">
        It is a single-point app that helps borrowers get easy and quick loans. The loan amount varies 
        from as low as Rs 2000 to as high as Rs 2,00,000. The process of applying for loans, completing 
        the paperwork, and the eventual disbursal of the loan amount is all done online. And the best part, 
        the next time you visit LoanFront for a loan, just log in and apply, and you will not have to go 
        through the entire loan process all over again. The entire loan process is transparent, ensuring 
        reliability and safe keeping of your data and information.
        </p>
        <a href="https://www.loanfront.in/" target="_blank" class="btn btnss btn-lg btn-primary">Visit Website</a>
        </div>
        </div>
    </div>
</div>
</section>
<!-- <br><br><br><br> -->

<!-- <section>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="card-block">
                    <h2 class="card-titlee">About <b style="color: #0078BC;"> PeopleFront</b></h2>
                    <br>
                    <p class="card-text" style="color:#425662">
                        The core of any company, business, country or economy is its people.
                        We at PeopleFront strive to do just that by connecting people and be a partner 
                        in the transformation journey. Transformation in the way People Invest as well as avail credit as borrowers. 
                        As we take small steps towards this journey, we look forward in you joining us!
                    </p>
                    <ul style="color:#425662;" class="card-bottom">
                    <li><img src="./img/Group 2.svg" style="margin: 9px; color:#425662;">Directly connect online Investors looking to <b>Invest & seeking Loan.</b></li>
                    <li><img src="./img/Group 2.svg" style="margin: 9px; color:#425662;">Secure funds transfer via Escrow Account.</li>
                    <li><img src="./img/Group 2.svg" style="margin: 9px; color:#425662;">Funds routed and managed by a Trustee (appointed by the bank).</li>
                    <li><img src="./img/Group 2.svg" style="margin: 9px; color:#425662;">Earn better returns on your Investments.</li>
                    <li><img src="./img/Group 2.svg" style="margin: 9px; color:#425662;">Direct transfer and easy repayments from <b>Borrower to Investor.</b></li>
                    <li><img src="./img/Group 2.svg" style="margin: 9px; color:#425662;">Complete Online Documentation.</li>
                    </ul>
                  </div>
          </div>
              <div class="col-md-6">
                <div class="card-imgs-bottom">
                </div>
              </div>
          </div>
    </div>
</section> -->
 <!-- <div class="demoo">
	<div class="row">
		<div class="col-md-12">
			<div id="myCarousel" class="carousel slide" data-ride="carousel">
				<div class="carousel-inner">
					<div class="carousel-item active">
						<div class="img-box"><img src="./img/1.jpg"></div>
						<p class="testimonial" style="color: white; justify-content: center;">Based on the experience as or business partner definitely there are many qualities that have evolved 
                        which has helped us manifolds. <br>- Accountability -Professionalism -Adherence to TATs Keep on growing and all the best. Cheers...</div>
					<div class="carousel-item">
						<div class="img-box"><img src="./img/2.jpg" alt=""></div>
						<p class="testimonial" style="color: white;">It is a good place to work and learn grow yourself fast. Senior colleagues are very cooperative .</p>					</div>
					<div class="carousel-item">
						<div class="img-box"><img src="./img/Component28.png" alt=""></div>
						<p class="testimonial" style="color: white;">I feel very proud to be one of the oldest employee of Capfront Technologies. My career has skied with the growth of the company. 
                            The extraordinary Knowledge Transfer<br> across the different streams of the project has benefitted lot of employee. I am really grateful to work in a place which makes us grow to greater heights.”</p>
					</div>
				</div>
				<a class="carousel-control-prev" href="#myCarousel" data-slide="prev">
					<img src="./img/next.png">
				</a>
				<a class="carousel-control-next" href="#myCarousel" data-slide="next">
					<img src="./img/nextyy.png">
				</a>
			</div>
		</div>
	</div>
</div> -->

<!-- <div class="container-fluid ourteam-base-div  p-0">
    <h1 class="our-team-container-heading pt-5">Meet Our Team</h1>
    <div class=" our-team-pogressbar">
        <hr class="our-team-accessory">
    </div>
    <p class="our-team-container-paragrph">We are a bunch of talented, and experienced professionals from diverse fields such as data analytics, IT and marketing to ensure efficient and effective deliverables to our clients and consumers.
        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
</div> -->
<!-- <div class="container-slider">
    <div class="next">
        <img src="./img/nextyy.png">
        </div>
        <div class="prev">
            <img src="./img/next.png">
        </div>
    <div class="autoplay">
        <div class="slidder-box">
            <img src="./img/Component25.png" class="ourteam-image">
            <h3 class="ourteam-image-div-heading">IVR Gowrinath</h2>
                <h4 class="ourteam-image-div-h4">CEO</h4>
                <p class="ourteam-image-div-paragrph">Our Founder and CEO, Mr. Gowrinath Raghava, is a seasoned professional
                    with 19+ years of experience in  Software development, Product/portfolio management, Human resource
                     and Operations management in various domains including financial services.</p>

                <div class="ourteam-sm-icons-div">
                    <div class="img-box-24">
                    </div>                        
                    <div class="img-box-25">
                    </div>
                    <div  class="img-box-26">
                    </div>
                </div>
        </div>
        <div class="slidder-box">
            <img src="./img/Component26.png" class="ourteam-image">
            <h3 class="ourteam-image-div-heading">Salsan Jose</h3>
            <h4 class="ourteam-image-div-h4">Tech Manager</h4>
            <p class="ourteam-image-div-paragrph">Salsan Jose is a young computer science master with 
                a towering repertoire built over a span of 13 years. He is adept at multiple technology 
                and solution platforms and has an unswerving passion to explore newer technologies 
                and Operations management in various domains</p>
                <div class="ourteam-sm-icons-div">
                    <div class="img-box-24">
                    </div>                        
                    <div class="img-box-25">
                    </div>
                    <div  class="img-box-26">
                    </div>
                </div>
        </div>
        <div class="slidder-box">
            <img src="./img/component20.png" class="ourteam-image">
            <h3 class="ourteam-image-div-heading">Prudhivi Kodali</h3>
            <h4 class="ourteam-image-div-h4">Software Architect</h4>
            <p class="ourteam-image-div-paragrph"> Prudhvi Kodali brings in an experience of more than 14 years working in the IT Industry.
                He has played varied roles in his 12 years of rich experience with Huawei Technologies.
                He has played various roles in his previous experience and has successfully completed</p>
            <div class="ourteam-sm-icons-div">
                <div class="img-box-24">
                </div>                        
                <div class="img-box-25">
                </div>
                <div  class="img-box-26">
                </div>

            </div>
        </div>
    </div>

</div> -->
<!--our team container end-->
<!-- <br><br><br><br> -->

<!--mapping image part end-->


 <!-- FOOTER STARTS -->
 <?php include_once('./includes/footer.php') ; ?>
    <!-- FOOTER ENDS -->
</body>
</html>