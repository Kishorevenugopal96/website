<!DOCTYPE html>
<html>

<head>
    <title>CapFront Technologies-Our Service</title>
   <!-- META TAGS STARTS -->
   <?php include_once('./includes/meta-tags.php');?>
    <!-- META TAGS ENDS -->
     <!-- CSS,JS FILES STARTS -->
     <?php include_once('./includes/head.php');?>
    <!-- CSS,JS FILES ENDS -->
     <link rel="stylesheet" href="service.css">
    <link rel="stylesheet" href="servicemobile.css">  

   <style>
       #active_service
       {
           font-weight:bold;
           text-decoration:none;
           color: #15a2f3 !important;
       }
   </style>

</head>

<body>

    <header id="headers" class="">
      <!-- MOBILE NAVIGATION STARTS -->
        <?php include_once("./includes/mobile-navigation-bar.php") ; ?>
      <!-- MOBILE NAVIGATION ENDS -->
        <div class="about-title">
            <p>Our Services</p>
        </div>
    </header><!-- End Header -->

    <!-- DESKTOP NAVIGATION STARTS -->
    <?php include_once("./includes/desk-top-navigation-bar.php");?>
   <!-- DESKTOP NAVIHATION ENDS -->

    <!--servicepage body start-->
    <!-- <div class="servicepage-body-container">
        <div class="row m-0">
            <div class="col-md-6 servies-para">
                <h3 class="body-first-div-heading"><span class="our-service-span">Our services</span> see what we do
                </h3>
                <p class="body-first-div-text my-4">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Laboriosam
                    dolorem, tempora autem veniam ex dolorum.
                    Quod ex sit atque reiciendis nihil vero labore quam ea error.
                    Velit modi, consectetur laudantium quidem reiciendis laborum culpa vero eligendi facere quas
                    obcaecati ipsam!
                </p>
                <p class="body-first-div-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Adipisci
                    non recusandae minus blanditiis sunt molestiae atque explicabo pariatur animi necessitatibus.
                </p>
            </div>
            <div class="col-md-6">
                <img src="./img/Group 1800.svg" class="group1800-image">
            </div>
        </div>
    </div> -->
    <!--servicepage body end-->
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-6" style="margin-top: 90px;">
                    <h3 class="body-first-div-heading"><span class="our-service-span">Our services,</span> what we do
                    </h3>
                    <p class="body-first-div-text my-4">CapFront takes pride in the varied, diverse, innovative and
                        tech-based solutions and services it provides to the financial institutions <b
                            style="color: #0078BC;">(Banks and NBFCs),</b>
                        which supports application-based lending. CapFront combines the latest cut-throat technology,
                        data analytics, marketing, and product development to meet the new-age challenges of running a
                        financial solution or product, and parallelly, it is leading a change in the evolvement of the
                        FinTech eco-system. CapFront provides a holistic <b style="color: #0078BC;">FinTech services</b>
                        to meet all the needs of
                        kickstarting, running and maintaining a financial solution all under one roof.
                </div>
                <div class="col-md-6">
                    <img alt="Our service from CapFront" src="./img/Group 1800.svg" class="group1800-image">
                </div>
            </div>
        </div>
    </section>
    <!--collection of rows div start-->
    <section>
        <div class="servicepage-body-container">
        <div class="row  container-2-rows mx-0">
        <div class="col-md-4 image-col-div" id="first-colm">
            <div class="image-div">
                <div class="icon-div">
                    <div class="imgLog"></div>
                </div>
            </div>
        </div>
        <div class="col-md-8 first-text-colmn">
            <h2 class="row-heading-1">UI/UX</h2>
            <p class="row-text-1">Web designing is an integral part of running any business solution. A poorly
                crafted web design could impact business, leading to revenue loss. We, at <b
                    style="color: #0078BC;">CapFront, employ creative,
                    skilled,</b> and <b style="color: #0078BC;">smart web designers</b> who can create effective
                interface, prioritising allthe stakeholders’
                interest. Our simple and effective web designs ensure that not only it is <b
                    style="color: #0078BC;">user-friendly,</b> but also
                helps the business in drawing and retaining users through its eye- grabbing pages, while
                customising
                the designs as per clients’ and users’ requirements.
            </p>
        </div>
        </div>
        <div class="row  container-3-rows mx-0">
        <div class="col-md-8  second-rows">
            <h2 class="row-heading-1">Product Engineering</h2>
            <p class="row-text-1">
            Proficient in process based product engineering starting from ideation, innovation, technical solution, 
            design implementation, validation and deployment using agile methodology to meet the challenges associated 
            with ever changing requirements of the digital world and utility based software delivery through Software as 
            a Service (SaaS).
            </p>
        </div>
        <div class="col-md-4  p-0" id="second-colm">
            <div class="image-div2">
                <div class="icon-div2">
                    <div class="imgLog-1"></div>
                </div>
            </div>
        </div>
        </div>
        <div class="row  container-3-rows mx-0">
        <div class="col-md-4 image-col-div" id="first-colm">
            <div class="image-div3">
                <div class="icon-div">
                    <div class="imgLog-2"></div>
                </div>
            </div>
        </div>
        <div class="col-md-8 first-text-colmn">
            <h2 class="row-heading-1">Frontend/Backend Developemnt</h2>
            <p class="row-text-1">
            Leader in development of rich frontend & strong backend services with awe view 
            of the application using modern frontend frameworks and a highly secured backend 
            services with a distributed micro services architecture that reaches out to all target 
            users in the competitive Fintech market.  
            </p>
        </div>
        </div>
        <div class="row  container-3-rows mx-0">
        <div class="col-md-8  second-rows">
            <h2 class="row-heading-1">Risk Analytics</h2>
            <p class="row-text-1">
        CapFront has developed varied techniques of identifying the risk through manual / semi-automated / 
        automated methods. The automated techniques involve in developing complex machine learning algorithm to 
        identify cutomer credit worthiness, market product trends, and identifying the different patterns in 
        disbursal / repayment behaviours. 
            </p>
        </div>
        <div class="col-md-4  p-0" id="second-colm">
            <div class="image-div2 image-div4">
                <div class="icon-div2">
                    <div class="imgLog-3"></div>
                </div>
            </div>
        </div>
        </div>
        <div class="row  container-3-rows mx-0">
        <div class="col-md-4 image-col-div" id="first-colm">
            <div class="image-div5">
                <div class="icon-div">
                    <div class="imgLog-4"></div>
                </div>
            </div>
        </div>
        <div class="col-md-8 first-text-colmn">
            <h2 class="row-heading-1">Big Data Analytics</h2>
            <p class="row-text-1">
        Advanced analytic techniques developed against very large, diverse data sets that include structured, 
        semi-structured and unstructured data, from different sources, and in different sizes. Our solutions can handle 
        high volume, high velocity or high variety. Application of Big Data techniques, creating data pipelines further 
        helps to solve real time data variations at a very large scale.
        </p>
        </div>
        </div>
        <div class="row  container-3-rows mx-0">
        <div class="col-md-8  second-rows">
            <h2 class="row-heading-1">Quality Control</h2>
            <p class="row-text-1">
            Quality is of highest priority and it’s never ending journey at CapFront. The Quality 
            starts at the very beginning of our product lifecycle and continues even after delivery 
            and support. We strongly believe in both Quality Assurance by adopting strong process in line 
            with international standards/ models and Quality Control by ensuring the product goes through a 
            strict verification and validation technics to identify defects early in the lifecycle.
            </p>
        </div>
        <div class="col-md-4  p-0" id="second-colm">
            <div class="image-div2-2 image-div4-2">
                <div class="icon-div2">
                    <div class="imgLog-6"></div>
                </div>
            </div>
        </div>
        </div>
        </div>
    </section>
    <!--collection of rows div end-->
    <!--servicepage product container start-->

    <!-- <div class="servicepage-product-container">
        <h1 class="servicepage-product-heading">Our Products</h1>
        <div class="servicecontnerpogressbar">
            <hr class="service-accessory">
        </div>

        <div class="row mx-0 margin-top">
            <div class="col-md-5 servicepage-product-img-cols">
                <img src="./img/Group 1314.svg" class="servicepage-product-img">
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-5 servicepage-product-img-cols">
                <img src="./img/Group 1715.svg" class="servicepage-product-img">
            </div>
        </div>
    </div> -->
    <br>
    <!-- <div class="service-page">
        <h1 class="servicepage-product-heading">Our Products</h1>
        <div class="servicecontnerpogressbar">
            <hr class="service-accessory">
        </div>
    </div> -->

    <!-- <div class="container">
        <div class="row mx-0 margin-top">
            <div class="col-md-3"></div>
            <div class="col-md-6 col-sm-6 col-xs-12 col-lg-6 servicepage-product-img-cols">
                <a href="https://www.loanfront.in/" target="_blank"><img src="./img/Group 1314.svg"
                        class="servicepage-product-img"></a>
            </div>
            <div class="col-md-3"></div>
        
        </div>
    </div> -->

    <!-- FOOTER STARTS -->
    <?php include_once('./includes/footer.php') ; ?>
    <!-- FOOTER ENDS -->
</body>

</html>