<!DOCTYPE html>
<html>

<head>
    <title>CapFront Technologies-ContactUs</title>
     <!-- META TAGS STARTS -->
 <?php include_once('./includes/meta-tags.php');?>
    <!-- META TAGS ENDS -->
     <!-- CSS,JS FILES STARTS -->
     <?php include_once('./includes/head.php');?>
    <!-- CSS,JS FILES ENDS -->

    <!-- GOOGLE RECAPTCHA STARTED -->
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
   <!-- GOOGLE RECAPTCHA ENDS -->

    

    <link rel="stylesheet" href="contactus.css">
    <link rel="stylesheet" href="contactusmobile.css">

    

<style>
    #sendBtn{
       
        border-radius: 0px solid white;
        outline:none;

    }
 .warnMessage{
          color: red;
          font-size: 14px;
          padding-top: 10px;
          font-weight: bold;
      }
      form input {
          border:none;
      }
#headers {
    height: 420px;
    transition: all 0.5s;
    z-index: 997;
    transition: all 0.5s;
    padding: 20px 0;
    background:white;
    background-image: url("./img/Group\ 1803@2x.png");
    background-size: cover;
  }

  .mobile-nav
  {
    position: absolute;
    right: -2px;
    left: 201px;
  }
  
  .mobile-nav-active .mobile-nav-toggle i
  {
    width: 159px;
    font-size: xx-large;
    margin-top: -20px;
    left: -144px;
    color:#3ec1d5;
    cursor: pointer;
  }
  #active_contact
  {
      font-weight:bold;
      text-decoration:none;
      color: #15a2f3 !important;
  }
  
    </style>


</head>

<body>

<header id="headers" class="">   
        <!-- MOBILE NAVIGATION STARTS -->
      <?php include_once("./includes/mobile-navigation-bar.php") ; ?>
      <!-- MOBILE NAVIGATION ENDS -->
    <div class="about-title">
    <p>Say Hello!</p>
    </div>
    <div class="about-para">
    <p class="para-contactus">
    Send us a message using the contact form.
    <br> Or use information below to reach us.
    </p>
    </div>
</header><!-- End Header -->   

   <!-- DESKTOP NAVIGATION STARTS -->
   <?php include_once("./includes/desk-top-navigation-bar.php");?>
   <!-- DESKTOP NAVIHATION ENDS -->

    <div class="contactpage-form-div">
        <div class="row mx-0 contactpage-form-div-row">
            <div class="col-md-7 contactpage-form-div-left-section">
                <h4 class="contactpage-form-heading-left">We Would Love to Hear from You</h4>
                <hr class="contactpage-form-heading-left-hr">
                <form class="contactpage-form-section" id="form">
                    <div class="form-group">
                        <input type="text" class="form-control contactpage-form-inputs"
                            placeholder="What is your name" id="name" name="name" >
                            <p class="warnMessage" id="nameWarning"></p>
                    </div>
                    <span id="nameloc"></span>
                    <div class="form-group">
                        <input type="email" class="form-control name  contactpage-form-inputs"
                            placeholder="What is your email address" id="email" name="email" >
                            <p class="warnMessage" id="emailWarning"></p>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control password contactpage-form-inputs"
                        placeholder="What is your mobile number" id="mobile"  name="mobile">
                        <p class="warnMessage" id="mobileWarning"></p>
                      
                    </div>
                    <span id="passwordloc"></span>
                    <div class="form-group">
                        <input type="text" class="form-control contactpage-form-inputs" placeholder="Subject" id="subject" name="subject" >
                        <p class="warnMessage" id="subjectWarning"></p>
                    </div>
                    <div class="form-group">
                        <!-- <input type="text" class="form-control contactpage-form-input-message-field " placeholder="Your Message"> -->
                        <textarea name="message" class="form-control contactpage-form-input-message-field" 
                        cols="55" rows="10" id="message" name="message" placeholder="Enter Your Message" ></textarea> 
                        <p class="warnMessage" id="messageWarning"></p>
                    </div>
                    
               <div class="form-group">
              
                
               </div>

               <div class="g-recaptcha" data-sitekey="6LfggCYcAAAAAD0pAUDicjWCvwo-q8KGdnqpQ3JA"></div>
            

                    <button type="submit" id="sendBtn" class="contactpage-form-section-button">Send Message</button>
                    <p style="text-align: center;" id="resultarea">555</p>
                </form>
            </div>
            <div class="col-md-5 contactpage-form-div-right-section">
                <h4 class="contactpage-form-right-section-heading">Contact</h4>
                <hr class="contactpage-form-heading-right-hr">

                <div class="contactpage-from-right-section-address-div">
                    <div class="row mx-0">
                        <div class="col-1 p-0">
                            <img src="./img/location2.png" class="contactpage-from-right-section-address-icon1">
                        </div>
                        <div class="col-11">
                        <p class="contactpage-from-right-section-text">
                        
                             <?php echo ADDRESS_LINE1 ?> ,<br>
                            <?php echo ADDRESS_LINE2 ?> ,<br>
                            <?php echo ADDRESS_LINE3 ?> ,<br>
                            <?php echo CITY  ?> -  <?php echo STATE ; ?> <?php echo PINCODE ?>
                        </p>
                        </div>
                    </div>
                    <br>
                    <div class="row mx-0">
                        <div class="col-1 p-0">
                            <img src="./img/mail.png" class="contactpage-from-right-section-address-icon1">
                        </div>
                        <div class="col-11">
                            <a href="mailto:info@capfront.in;"><p class="contactpage-from-right-section-text" 
                                style="font-size: 14px;"><?php echo CAPFRONT_OFFICIAL_EMAIL?></p></a>
                        </div>
                    </div>
                    <br>
                    <div class="row mx-0">
                        <div class="col-1 p-0">
                            <img src="./img/phone (2).png" class="contactpage-from-right-section-address-icon1">
                        </div>
                        <div class="col-11">
                            <a href="tel:<?php echo CAPFRONT_LANDLINE_NUMBER?>;"><p class="contactpage-from-right-section-text contact-number" style="font-size: 14px;"><?php echo CAPFRONT_LANDLINE_NUMBER?></p></a>
                        </div>
                    </div>
                </div>
                <p class="contactpage-from-right-paragrph">For partnership and corporate enquiries, please lorem ipsum dolor sit amet, consectetur
                    adipiscing elit. Sed condimentum scelerisque dui, vitae egestas magna porta non. Vivamus orci
                    nunc, porta eu.adipiscing elit. Sed condimentum scelerisque dui, vitae egestas magna porta non.</p>
            </div>
        </div>
    </div>

    <br><br><br><br>

    <!-- <div class="container-fluid mapimage-div">
    </div> -->
   
    <div class="container-fluid">
        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12"> 
            <iframe src="https://www.google.com/maps/embed?pb=!1m22!1m8!1m3!1d3888.1792557542226!2d77.63433596482182!3d12.960378740863229!3m2!1i1024!2i768!4f13.1!4m11!3e6!4m3!3m2!1d12.959743999999999!2d77.6372224!4m5!1s0x3bae119be745a67f%3A0xb681ff19721df686!2scapfront%20technologies!3m2!1d12.9608593!2d77.6365181!5e0!3m2!1sen!2sin!4v1631780185450!5m2!1sen!2sin" width="100%" height="420" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
        </div>
    </div>
    <br><br>
    <!--mapping image part end-->
    
    <!-- FOOTER FILES STARTS -->
    <?php include_once('./includes/footer.php');?>
    <!-- FOOTER FILES ENDS -->
</body>

<script>
        function showErrorMessage(id,message,paraId){
            $(id).css('border', '1px solid red');
            $(paraId).html(message);
        }
    </script>
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>

<script>
       $(document).ready(function(){

        $("#name").keyup(function () {
        var name = $("#name").val();
        capitalizeFirstLetter = name.charAt(0).toUpperCase() + name.slice(1);
        $("#name").val(capitalizeFirstLetter);
    });

        /*
            $("#mobile").keyup(function() {
            var len = $(this).val().length;
            if(len>10){
                var mobileNumber = $("#mobile").val();
                $("#mobile").val(mobileNumber.slice(0,10));
            }
            });
        */
        
          $("#form").on('submit',function(e){
              e.preventDefault();
        

            var isFormValid = false;

            var nameRegex = '^[A-Za-z ]{3,100}$';
            var emailReg = '^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$';
            var mobileRg='^[0-9]{10}$';

            var name = $("#name").val();
            var email = $("#email").val();
            var mobile = $("#mobile").val();
            var message = $("#message").val();
            var subject = $("#subject").val();

            $(".warnMessage").each(function(){
                $(this).html("");
            });
           
            if (name.match(nameRegex)) {
                isFormValid = true;
                $("#nameWarning").html("");
                $("#name").css('border', '1px solid #0078BC');
            }

            if (email.match(emailReg)) {
                isFormValid = true;
                $("#nameWarning").html("");
                $("#email").css('border', '1px solid #0078BC');
            }

            if (mobile.match(mobileRg)) {
                isFormValid = true;
                $("#mobileWarning").html("");
                $("#mobile").css('border', '1px solid #0078BC');
            }

            if (subject.match(nameRegex)) {
                isFormValid = true;
                $("#subjectWarning").html("");
                $("#subject").css('border', '1px solid #0078BC');
            }

            if(message != ""){
                isFormValid = true;
                $("#messageWarning").html("");
                $("#message").css('border', '1px solid #0078BC');
            }

            if (!name.match(nameRegex)) {
                isFormValid = false;
                var id = "#name";
                var paraId = "#nameWarning";
                var message = "<sup>*</sup> <?php echo NAME_ERROR_MSG; ?>";
                showErrorMessage(id,message,paraId);
                e.preventDefault();
            } 

            if (!email.match(emailReg)) {
                isFormValid = false;
                var id = "#email";
                var paraId = "#emailWarning";
                var message = "<sup>*</sup> <?php echo EMAIL_ERROR_MSG; ?>";
                showErrorMessage(id,message,paraId);
                e.preventDefault();   
            }


            if(!mobile.match(mobileRg)){
                isFormValid = false;
                var id = "#mobile";
                var paraId = "#mobileWarning";
                var message ="<sup>*</sup> <?php echo MOBILE_ERROR_MSG; ?>";
                
                showErrorMessage(id,message,paraId);
                e.preventDefault();
            }
           

            if(!subject.match(nameRegex)){
                isFormValid = false;
                var id = "#subject";
                var paraId = "#subjectWarning";
                var message = "<sup>*</sup> <?php echo SUBJECT_ERROR_MSG; ?>";
                showErrorMessage(id,message,paraId);
                e.preventDefault();
            }

            if(message == ""){
                isFormValid = false;
                var id = "#message";
                var paraId = "#messageWarning";
                var message = "<sup>*</sup> <?php echo MESSAGE_ERROR_MSG; ?>";
                showErrorMessage(id,message,paraId);
                e.preventDefault();  

            } else if(message.length < 15){
                isFormValid = false;
                var id = "#message";
                var paraId = "#messageWarning";
                var message = "<sup>*</sup> <?php echo MESSAGE_LENGTH_ERROR_MSG; ?>";
                showErrorMessage(id,message,paraId);
                e.preventDefault(); 
                
            } 
            //{name:name,email:email,mobile:mobile,subject:subject,message:message},

            if(isFormValid == true){
                $("#resultarea").html(`<span style="color:#0078BC">Please wait..</span>`);
                e.preventDefault();
                $.ajax({
                        type: "POST",
                        url: "process/contact-page-form.php",
                        data: $("#form").serialize(), 
                        dataType:"json",
                        cache: false,
                        success: function(data){
                            console.log(data);
                            var response = JSON.parse(JSON.stringify(data));
                            if(response.status != 200){
                                $("#resultarea").html(`<span style="color:red">${response.message}</span>`);
                            } else{
                                $("#resultarea").html(`<span style="color:green">${response.message}</span>`);
                            }
                        }
                });
            }

          });
       });
   </script>
</html>