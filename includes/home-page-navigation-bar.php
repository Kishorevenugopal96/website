<?php
  
    include_once("./config.php");
?>
<?php



?>

<!-- NAVIGATION BAR STARTS -->
<div class="container-fluid d-flex">
          <div class="logo mr-auto">
            <a href="<?php echo HOME_PAGE_LINK ?>"><img src="<?php echo COMPANY_LOGO ?>" class="logo-img"></a>
          </div>
          <nav class="nav-menu d-none d-lg-block">
            <ul>
                <li class="active"><a href="<?php echo HOME_PAGE_LINK ?>">Home<img src="./img/browser.png" style="margin-left: 9px;font-size: 5px;height: 18px;" class="home-icon"></a></li>
                <li class="names-icon"><a href="<?php echo ABOUT_US_PAGE_LINK ?>">About Us <img src="./img/user (2).png" style="margin-left: 9px;height: 18px;" class="home-icon"></a></li>
                <li ><a href="<?php echo OUR_SERVICE_PAGE_LINK ?>">Our Services<img src="./img/customer-support.png" style="margin-left: 10px;height: 18px;" class="home-icon"></a></li>
                <li><a href="<?php echo OUR_PRODUCTS_PAGE_LINK ?>">Our Products<img src="./img/return-on-investment.png" style="height: 18px;" class="home-icon product-icon"></a></li>
                <!-- <li><a href="blog.html">Blogs<img src="./img/copywriting.png" style="margin-left: 9px;height: 18px;" class="home-icon"></a></li> -->
                <li><a href="<?php echo CONTACT_US_PAGE_LINK ?>">Contact Us<img src="./img/phone (1).png" style="margin-left: 9px;height: 18px;" class="home-icon"></a></li>
            </ul>
          </nav><!-- .nav-menu -->
        </div>
          <div class="container-fluid text-white homehighlightcontent">
        <h1 class="h1-homepage">Leader. <span style="color:#0078BC; font-weight: bold;" class="footer-span">Innovator.</span> Pioneer.</h1>
        <p class="para-homepages">
        CapFront is leading the way FinTech market is evolving now, always innovating to bridge the gap between lender and borrower,and pioneering in using advanced technologies 
        and data analytics to boost the FinTech eco-system.
        </p>
        <!-- <div class="text-center"><button onclick="myFunction()" id="myBtn">Find Out More</button></div> -->
</div>

<!-- NAVIGATION BAR ENDS -->