 <?php  include_once("config.php");?>
 <!-- Start Header -->

    <div class="container-fluid d-none d-flex">
        <div class="logo mr-auto " style="margin-top: 5px;">
            <a href="capfronthome.html"><img src="./img/logo.png" class="logo-img"></a>
        </div>
        <nav class="nav-menu d-none d-lg-block">
       

       <ul>
            <li><a href="<?php echo HOME_PAGE_LINK ?>">Home<img src="./img/browser.png" style="margin-right: 0px;margin-left: 9px;height: 21px;"></a></li>
            <li class="names-icon active_about"><a href="<?php echo ABOUT_US_PAGE_LINK;?>">About Us<img src="./img/user (2).png" style="margin-right:0 px;margin-left: 9px;height: 21px;"></a></li>
            <li class="active_service"><a href="<?php echo OUR_SERVICE_PAGE_LINK; ?>">Our Services<img src="./img/customer-support.png" style="margin-right: px;margin-left: 13px;height: 21px;"></a></li>
            <li class="active_product"><a href="<?php echo OUR_PRODUCTS_PAGE_LINK;?>">Our Products<img src="./img/return-on-investment.png" style="margin-left: 13px;height: 21px;"></a></li>
          <!-- <li><a href="blog.html">Blogs<img src="./img/copywriting.png" style="margin-right: 0px;margin-left: 9px;height: 21px;"></a></li> -->
            <li class="active_contact"><a href="<?php echo CONTACT_US_PAGE_LINK ?>">Contact Us<img src="./img/phone (1).png" style="margin-right: 0px;margin-left: 9px;height: 21px;"></a></li>
        </ul>
        </nav>
        
        <!-- .nav-menu -->
    </div>
  


<nav class="fixed-top">
    <div class="container-fluid d-flex">
        <div class="logo mr-auto" style="margin-top: 15px;">
            <a href="<?php echo HOME_PAGE_LINK ?>"><img src="./img/logo.png"></a>
        </div>
    <div class="col-12 py-3">
      <div class="navWrapper" id="Demo">
        <div class="nav-dv">
            <ul class="nav-col">
            <li><a href="<?php echo CONTACT_US_PAGE_LINK ?>" style="margin-right: px;">Contact us</a></li>
            <!-- <li><a href="blog.html" style="margin-right: px;">Blog</a></li> -->
            <li><a href="<?php echo OUR_PRODUCTS_PAGE_LINK;?>" style="margin-right: px;">Our Products</a></li>
            <li><a href="<?php echo OUR_SERVICE_PAGE_LINK; ?>"style="margin-right: px;">Our Services</a></li>
            <li class="active"><a href="<?php echo ABOUT_US_PAGE_LINK;?>" style="margin-right: px;color: #0078BC;">About us</a></li>
            <li><a href="<?php echo HOME_PAGE_LINK ?>" style="margin-right: px;">Home</a></li>
            </ul>
        </div>
      </div>
    </div>
    </div>
</nav>
<!-- End Header -->