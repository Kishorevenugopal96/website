<?php include_once("./config.php") 
?>
<!-- GOOGLE MAP STARTS -->
<!-- <div class="container-fluid">
        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12"> 
            <iframe src="https://www.google.com/maps/embed?pb=!1m22!1m8!1m3!1d3888.1792557542226!2d77.63433596482182!3d12.960378740863229!3m2!1i1024!2i768!4f13.1!4m11!3e6!4m3!3m2!1d12.959743999999999!2d77.6372224!4m5!1s0x3bae119be745a67f%3A0xb681ff19721df686!2scapfront%20technologies!3m2!1d12.9608593!2d77.6365181!5e0!3m2!1sen!2sin!4v1631780185450!5m2!1sen!2sin" width="100%" height="420" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
        </div>
</div> -->

<!-- GOOGLE MAP ENDS -->


<section class="container-fluid">
    <div class="card  footercard">
        <div class="row p-3">
            <div class="col-md-4 capfrontlogo-1">
                <img src="<?php echo COMPANY_FOOTER_LOGO; ?>" class="capfrontlogo">
            </div>
            <div class="col-md-4 footer-card-columns">
                <h1  style="color: #0078BC; font-weight: bold;font-size: 30px;" class="footer-h2">About CapFront</h1>
                <a href="<?php echo ABOUT_US_PAGE_LINK?>" target="_blank" style="text-decoration: none;"><small  class="footer-text">What is Capfront?</small></a><br>
                <a href="<?php echo OUR_SERVICE_PAGE_LINK ?>" target="_blank" style="text-decoration: none;"><small class="footer-text">Our Services</small></a><br>
                <a href="<?php echo OUR_PRODUCTS_PAGE_LINK ?>" target="_blank" style="text-decoration: none;"><small  class="footer-text">Our Products</small></a><br>
                <a href="<?php echo CONTACT_US_PAGE_LINK ?>" target="_blank" style="text-decoration: none;"><small  class="footer-text">Contact Us</small></a>
            </div>
            <div class="col-md-4 footer-card-columns">
                <h1 class="card-h1-columns footer-h2" style="font: normal normal bold  STIXIntegralsD; color: #0078BC;font-weight: bold;">Communication Address</h1>
                <p style="color: #425662;font-weight: bold; font-size: 13px;">
                <?php echo ADDRESS_LINE1 ?> 
                <?php echo ADDRESS_LINE2 ?> 
                <?php echo ADDRESS_LINE3 ?> 
                <?php echo CITY  ?> -  <?php echo STATE ; ?> <?php echo PINCODE ?> 
               </p>                
                <div class="fotter-icons-div" style="margin-left: -14px;margin-top:11px;">
                   <a href="<?php echo CAPFRONT_OFFICIAL_LINKEDIN; ?>" target="_blank"><img src="./img/linkedin.png" class="icons"></a>
                   <a href="<?php echo CAPFRONT_OFFICIAL_TWITTER; ?>" target="_blank"><img src="./img/twitter.png" class="icons"></a>
                   <a href="<?php echo CAPFRONT_OFFICIAL_FACEBOOK;?>" target="_blank"><img src="./img/facebook.png" class="icons"></a>
                </div>
                <h1 class="footer-number" style="color:green;margin-left:-4px;font: normal normal bold 30px/27px Rajdhani;">
                <a style="text-decoration:none;" href="tel:<?php echo CAPFRONT_LANDLINE_NUMBER; ?>" 
                ><?php echo CAPFRONT_LANDLINE_NUMBER; ?></a>
                </h1>
                <p class="footer-small-div">MON-SAT:10:00 A.M-07:00 P.M</p>
            </div>
            <hr class="hrs">
        </div>
    </div>
    </section>


    <div class="container-fluid mt-5">
    <div class="row">
    <div class="col-md-6">
        <P class="footerparagrph">Member Of <span class="footer-span"><a class="footerparagrph-link"
        href="">FaceOfIndia.org</a></span> and <span class="footer-span"><a class="footerparagrph-link"
        href="">Digital Lenders Association</a></span> Of India</P>
        <a href="https://faceofindia.org/" target="_blank"><img src="./img/Group 1734.png" class="footer-imgage"></a>
        <a href="https://www.dlai.in/"  target="_blank"><img src="./img/Group 1737.png" class="footer-imgage"></a>
    </div>
    <div class="col-md-6">
        <img src="./img/mmm.png" class="footer-image">
    </div>
    </div>
    </div>


    <script src="capfronthome.js"></script>
    <script src="main.js"></script>
    <!-- <script src="capfronthome.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
    integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
    crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
    integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
    crossorigin="anonymous"></script>
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
    <script src="assets/vendor/nivo-slider/js/jquery.nivo.slider.js"></script>
    <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
    <script src="assets/vendor/venobox/venobox.min.js"></script>