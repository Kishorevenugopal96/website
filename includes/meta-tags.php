<?php 
define(
            "META_DESCRIPTION", "CapFront Technologies is a private start-up based in Bengaluru with the 
            focus on providing data analytics, digital marketing services, product development."
);
?>


    <meta name="description" content="<?php echo META_DESCRIPTION;?>">
    <!-- Twitter meta-->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:site" content="@GoLoanFront">
    <meta property="twitter:creator" content="@GoLoanFront">
    <!-- Open Graph Meta-->
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="CapFront Technologies">
    <meta property="og:title" content="CapFront Technologies is a privately held information technology start-up based in Bengaluru, India.">
    <meta property="og:url" content="http://pratikborsadiya.in/blog/vali-admin">
    <meta property="og:image" content="http://pratikborsadiya.in/blog/vali-admin/hero-social.png">
    <meta property="og:description" content="<?php echo META_DESCRIPTION;?>">
    <link rel="icon" type="image/png" href="./img/capfrontlogo.jpg"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  
  