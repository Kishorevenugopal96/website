<?php include_once("./config.php"); ?>



<div class="container d-flex">
            <div class="logo mr-auto" style="margin-top: 0px;">
                <a href="<?php echo HOME_PAGE_LINK?>"><img src="./img/logo.png" class="logo-img"></a>
            </div>
            <nav class="nav-menu d-none d-lg-block">
                <ul>
                    <li><a href="<?php echo HOME_PAGE_LINK?>">Home<img src="./img/browser.png"
                                style="margin-left: 9px;height: 21px;"></a></li>
                    <li><a href="<?php echo ABOUT_US_PAGE_LINK; ?>">About Us <img src="./img/user (2).png"
                                style="margin-left: 9px;height: 21px;"></a></li>
                    <li><a href="<?php echo OUR_SERVICE_PAGE_LINK ?>">Our Services<img src="./img/customer-support.png"
                                style="margin-left: 9px;height: 21px;"></a></li>
                    <li><a href="<?php echo OUR_PRODUCTS_PAGE_LINK;?>">Our Products<img src="./img/return-on-investment.png"
                                style="margin-left: 5px;height: 21px;"></a></li>
                    <!-- <li><a href="blog.html">Blogs<img src="./img/copywriting.png" style="margin-left: 9px;height: 21px;"></a></li> -->
                    <li><a href="<?php echo CONTACT_US_PAGE_LINK ?>">Contact Us<img src="./img/phone (1).png"
                                style="margin-left: 9px;height: 21px;"></a></li>
                </ul>
            </nav><!-- .nav-menu -->
        </div>

        