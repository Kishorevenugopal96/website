<?php include_once("config.php");?>




    <nav class="fixed-top">
        <div class="container-fluid d-flex">
            <div class="logo mr-auto" style="margin-top: 15px;">
                <a href="<?php echo HOME_PAGE_LINK?>"><img src="./img/logo.png"></a>
            </div>
            <div class="col-12 py-3">
                <div class="navWrapper" id="Demo">
                    <div class="nav-dv">
                        <ul class="nav-col">
                            <li><a id="active_contact"  href="<?php echo CONTACT_US_PAGE_LINK ?>">Contact us</a></li>
                            <!-- <li><a href="blog.html" style="margin-right: px;">Blog</a></li> -->
                            <li><a id="active_product" href="<?php echo OUR_PRODUCTS_PAGE_LINK;?>" style="margin-right: px;">Our Products</a></li>
                            <li><a id="active_service" href="<?php echo OUR_SERVICE_PAGE_LINK ?>" style="margin-right: px;">Our  Services</a></li>
                            <li><a id="active_about" href="<?php echo ABOUT_US_PAGE_LINK; ?>" style="margin-right: px;">About us</a></li>
                            <li><a id="active_home" href="<?php echo HOME_PAGE_LINK?>" style="margin-right: px;">Home</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </nav>